const supertest = require('supertest')
const app = require('../server.js')
const request = supertest(app)

describe('GET /', function(done) {
   it('displays "IGTI - Infraestrutura de a..."', function(done) {
     // The line below is the core test of our app.
     request.get('/')
      .expect('<title>IGTI - Infraestrutura de aplicacoes WEB</title> <h1>IGTI - Infraestrutura de aplicacoes WEB</h1> <P> Aluno: Bruno de miranda Cintra </p>')
      .then(() => done())
      .catch(done)
   })
 })